var $ = require('min-jquery');
var d3 = require('d3');
/**
 * Created by developer on 11.05.2017.
 */
$(function() {


    var leftHand = d3.select(".js-leftHand");
    var rightHand = d3.select(".js-rightHand");
    var hands = d3.select(".js-hands");

    hands.selectAll("path")
        .attr("stroke-dashoffset", "0%")
        .attr("stroke-dasharray", "0%, 100%")
        .style("opacity", 1);

    leftHand.selectAll("path")
        .attr("stroke-dashoffset", "0%")
        .attr("stroke-dasharray", "0%, 100%")
        .transition()
        .duration(600)
        .ease(d3.easeCircleInOut)
        .attr("stroke-dasharray", "100%, 0%");

    rightHand.selectAll("path")
        .attr("stroke-dashoffset", "0%")
        .attr("stroke-dasharray", "0%, 100%")
        .transition()
        .duration(600)
        .ease(d3.easeCircleInOut)
        .attr("stroke-dasharray", "100%, 0%");

    leftHand.handshake = function() {
        return leftHand
            .transition()
            .duration(800)
            .ease(d3.easeCircleInOut)
            .style("left", "50%")
            .style("opacity", 0);
    };

    rightHand.handshake = function() {
        return rightHand
            .transition()
            .duration(800)
            .ease(d3.easeCircleInOut)
            .style("right", "50%")
            .style("opacity", 0);
    };

    hands.handshake = function() {
        return hands
            .transition()
            .delay(1400)
            .duration(200)
            .ease(d3.easeLinear)
            .style("margin-top", "-5px")
            .transition()
            .duration(400)
            .ease(d3.easeBackIn)
            .style("margin-top", "5px")
            .transition()
            .duration(200)
            .ease(d3.easeLinear)
            .style("margin-top", "0px")
    };


    $('.ag-form__submit').click(function(){
        leftHand.handshake();
        rightHand.handshake();
        console.log('cliked');
        d3.select(".js-hide")
            .transition()
            .duration(600)
            .ease(d3.easeCircleIn)
            .style("opacity", 0);

        d3.select(".js-circle")
            .append("circle")
            .attr("cx", 100)
            .attr("cy", 100)
            .attr("r", 0)
            .transition()
            .duration(1000)
            .delay(400)
            .ease(d3.easeElasticOut)
            .attr("r", 60);

        hands.selectAll("path")
            .transition()
            .duration(600)
            .delay(800)
            .ease(d3.easeCircleInOut)
            .attr("stroke-dasharray", "100%, 0%");

        hands.handshake();

        d3.select(".js-thanks")
            .transition()
            .duration(1000)
            .delay(1500)
            .ease(d3.easeCircleInOut)
            .style("opacity", 1);

        return false;
    });
});