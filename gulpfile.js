/**
 * Created by developer on 23.01.2017.
 */
var gulp = require('gulp');
var data = require('gulp-data');
var gulpBrowser = require("gulp-browser");
var watch = require('gulp-watch');
var stylus = require('gulp-stylus');
var browserSync = require('browser-sync').create();
var nunjucks = require('gulp-nunjucks');
var nunjucksRender = require('gulp-nunjucks-render');
var pretty  = require('gulp-html-prettify');
var uglify = require('gulp-uglify');



gulp.task('default', function() {

	gulp.run (['compile-styl']);
	gulp.run (['compile-js']);
	gulp.run (['compile-html']);

	browserSync.init({
		server: "./app/"
	});

	watch('./dist/css/**/*.styl', function(){
		gulp.run (['compile-styl']);
	});

	watch('./dist/js/**/*.js', function(){
		gulp.run (['js-watch']);
	});

	watch("./dist/templates/**/*.njk", function() {
		gulp.run (['template-watch']);
	});

});


gulp.task('compile-styl', function(){
	return gulp.src('./dist/css/style.styl')
		.pipe(stylus())
		.pipe(gulp.dest('./app/css/'))
		.pipe(browserSync.stream());
});

gulp.task('compile-js', function(){
	return gulp.src('./dist/js/*.js')
		.pipe(gulpBrowser.browserify())
		// .pipe(uglify())
		.pipe(gulp.dest('./app/js/'));
});

gulp.task('compile-html', function(){
	return gulp.src('./dist/templates/index.njk')
		.pipe(nunjucks.compile())
		.pipe(nunjucksRender())
		.pipe(pretty({indent_char: '', indent_size: 4}))
		.pipe(gulp.dest('./app/'));

});

gulp.task('template-watch', ['compile-html'], function (done) {
	browserSync.reload();
	done();
});

gulp.task('js-watch', ['compile-js'], function (done) {
    watch('./dist/js/*.js', function(){
        gulp.run (['compile-js']);
    });
});

gulp.task('watch-styl', ['compile-styl'], function(){
	watch('./dist/css/**/*.styl', function(){
		gulp.run (['compile-styl']);
	});
});